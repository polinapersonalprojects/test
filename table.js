window.addEventListener("DOMContentLoaded", () => {
  let data = [];

  const tBody = document.getElementById("table-body");
  const addEmployeeBtn = document.getElementById("add-btn");
  const logData = document.getElementById("submit-btn");
  const fullNameEmployee = document.getElementById("full-name");
  const ageEmployee = document.getElementById("age");
  const competenciesEmployee = document.getElementById("competencies");
  competenciesEmployee.value = '';
  const formElement = document.getElementById("my_form");

  const addRow = (e) => {
    e.preventDefault();

    validateName();
    validateAge();
    validateCompetencies();

    if (validateName() && validateAge() && validateCompetencies()) {
      createEmployee();
      addEmployee();
      fullNameEmployee.value = '';
      ageEmployee.value = '';
      competenciesEmployee.value = '';
    }
  }

  const createEmployee = () => {
    const formData = new FormData(formElement);
    const fullNameData = formData.get('full-name');
    const ageData = formData.get('age');
    const competenciesData = formData.get('competencies');
    const positionData = formData.get('position');

    const employee = {
      fullName: fullNameData,
      position: positionData,
      age: ageData,
      competencies: competenciesData,
      id: data.length + 1,
    }

    data.push(employee);
  }

  const addEmployee = () => {
    data.slice(-1).forEach(({ fullName, position, age, competencies, id }) => {
      const tRow = document.createElement("tr");
      tRow.id = id

      const buttonWrapper = document.createElement("div");
      buttonWrapper.className = 'd-flex p-2'
      const buttonElement = document.createElement("button");
      buttonElement.innerText = '-';
      buttonElement.className = 'btn'
      tRow.innerHTML = `<td>${fullName}</td> <td>${position}</td> <td>${age}</td> <td>${competencies}</td>`;
      buttonWrapper.appendChild(buttonElement);
      tRow.appendChild(buttonWrapper);
      tBody.appendChild(tRow);

      buttonElement.addEventListener('click', deleteRow(id));
    });
  };

  const validateName = () => {
    const formData = new FormData(formElement);
    const fullName = formData.get('full-name');

    const validName = document.getElementById('name-validation');

    if (fullName.length === 0) {
      validName.classList.add('invalid-name');
      return false;
    } else if (fullName.match(/\d/g)) {
      validName.classList.add('invalid-name');
      validName.innerText = 'Используйте только кириллицу';
      return false;
    } else {
      validName.classList.remove('invalid-name');
      return true;
    }
  }

  const validateAge = () => {
    const formData = new FormData(formElement);
    const age = formData.get('age');

    const validAge = document.getElementById('age-validation');

    if (age.length === 0) {
      validAge.classList.add('invalid-age');
      return false
    } else if (age < 0) {
      validAge.classList.add('invalid-age');
      validAge.innerText = 'Возраст не может быть отрицательным'
      return false
    } else {
      validAge.classList.remove('invalid-age');
      return true;
    }
  }

  const validateCompetencies = () => {
    const formData = new FormData(formElement);
    const competencies = formData.get('competencies');

    const validCompetencies = document.getElementById('competencies-validation');

    if (competencies.length === 0) {
      validCompetencies.classList.add('invalid-competencies');
      return false
    } else {
      validCompetencies .classList.remove('invalid-competencies');
      return true;
    }
  }

  addEmployeeBtn.addEventListener('click', addRow);

  const submitData = () => {
    const dataObj = Object.assign({}, data);
    return console.log(JSON.stringify(dataObj));
  };

  const deleteRow = (id) => {
    return () => {
      const currentRow = document.getElementById(id);
      currentRow.remove();
      data = data.filter(el => el.id === id);
    }
  };

  logData.addEventListener('click', submitData);
});




